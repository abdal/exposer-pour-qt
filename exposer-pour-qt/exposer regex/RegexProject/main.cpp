#include <iostream>
#include <regex>
#include <string>

using namespace std;

bool estMotValide(const string &mot) {
    // Expression régulière pour vérifier le motif du mot
    regex motif("^[A-Z][a-zA-Z0-9]{2}[0-9]$"); // estmotvalide doit grace au regex comporter au début une maj 2 caractere (maj min      //chiffre et a la fin un chiffre de 0-9)

    // ^ signifie au début de la chaine
    // [A-Z] sinifie qu'il définie un classe de caractere (de a à z en majuscule)
    /* [a-zA-Z0-9]{6} sinifie qu'il définie un classe de caracterede a à z en minuscule
    et en majuscule mais aussi qu'il include les chiffre de 0-9 le {6} signifie pendend 6 caractere */
    // [0-9]$ efin signifie qu'il doit avoir un chiffre de [0-9] et le $ signifie a la fin


    // Vérification de la taille  du mot
    if (mot.length() != 4) { // lenght veux dire la taille
        cout << "Erreur : le mot doit avoir exactement 4 caracteres." << endl;
        return false;
    }

    // Vérification du motif motif c'est voir si le mot termine ou non par les condition cité avant 
    if (!regex_match(mot, motif)) { // le ! veux dire que c'est un bool qui retourne qlqchs (séance du 26/03)
        // Vérification si la première lettre est une majuscule
        if (!isupper(mot[0])) { // [0] veux dire que c'est le premier caractere
            cout << "Erreur : le mot doit comencer par une majuscul." << endl;
        }
        // Vérification si le dernier caractere est un chiffre
        else if (!isdigit(mot.back())) {
            cout << "Erreur : le mot doit se terminer par un chiffre." << endl;
        }
        return false;
    }
    return true;
}

int main() {
    string mot;

    while (true) {
        cout << "Entrez un mot (4 caractere, une majuscul au debut et un chiffre a la fin et pas de caractere speciaux  : ";
        cin >> mot; // on tape le mot 

        if (estMotValide(mot)) {
            cout << "Mot valide !" << endl;
        } else {
            cout << "Veuiller retaper le mot de passe (il contien peut etre des caractere speciaux) ." << endl; // c'est pour moi car le mot contion des caractere non autauriser
        }
    }

    return 0;
}
